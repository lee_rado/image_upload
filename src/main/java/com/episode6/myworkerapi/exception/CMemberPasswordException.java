package com.episode6.myworkerapi.exception;

public class CMemberPasswordException extends RuntimeException {
    public CMemberPasswordException(String msg, Throwable t) {
        super(msg,t);
    }
    public CMemberPasswordException(String msg) {
        super(msg);
    }
    public CMemberPasswordException() {
        super();
    }
}
