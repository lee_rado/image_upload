package com.episode6.myworkerapi.exception;

public class CMemberUsernameException extends RuntimeException {
    public CMemberUsernameException(String msg, Throwable t) { super(msg,t); }
    public CMemberUsernameException(String msg) { super(msg); }
    public CMemberUsernameException() { super(); }
}
