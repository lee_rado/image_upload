package com.episode6.myworkerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class MyWorkerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyWorkerApiApplication.class, args);
    }

    //보안 담당자 추가
    //Spring Security의 보안 설정을 구성하기 위해 PasswordEncoder를 Bean으로 등록
    //PasswordEncoder는 사용자 비밀번호를 암호화하고, Spring Security와 함께 사용된다.
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

}
