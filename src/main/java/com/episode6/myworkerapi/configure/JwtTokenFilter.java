package com.episode6.myworkerapi.configure;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtTokenFilter extends GenericFilterBean {
    //HTTP 요청에서 JWT 토큰을 추출하고 유효성을 검사하여 사용자의 인증 및 권한 부여를 담당
    //Spring Security의 필터 체인에 등록되어 모든 요청에 대해 JWT 토큰을 처리하고, 인증된 사용자의 정보를 설정
    //이를 통해 Spring Security에서 JWT를 사용한 인증 및 권한 부여를 구현할 수 있음.
    private final JwtTokenProvider jwtTokenProvider;
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        String token = "";
        if (tokenFull == null || !tokenFull.startsWith("Bearer ")) {
            chain.doFilter(request, response);
            //리턴 타입이 void인데 리턴이 있으면 끝나서 빠져나가는 것이다.
            return;
        }
        //trim 혹시모를 에러를 방지해줌
        token = tokenFull.split(" ")[1].trim();
        if (!jwtTokenProvider.validateToken(token)) {
            chain.doFilter(request,response);
            return;
        }
        //일회용 출입증 발급
        Authentication authentication = jwtTokenProvider.getAuthentication(token);
        //다같이 보려고 주변환경(context)를 고정(holder)시켜놓는 것 - 일회용 출입증을 고정시킨다.
        //왔다갔다하면 유실되기에 요청에 한해서 잠시 고정된다.
        //일회용 출입증 주세요가 아니라 일회용 출입증 확인해볼게요로 된다.
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }
}
