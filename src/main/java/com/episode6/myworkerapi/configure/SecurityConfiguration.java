package com.episode6.myworkerapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;


@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomAuthenticationEntryPoint entryPoint;
    private final CustomAccessDeniedHandler accessDenied;

    @Bean
    //Cors를 풀어준 것
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        config.setAllowedOrigins(List.of("*"));
        //매핑의 이름인데 options가 가장 중요하다. api 호출할 때 실질적으로 몇번하나? 2번 => 프리플라이트
        config.setAllowedMethods((List.of("GET","POST","PUT","DELETE","PATCH","OPTIONS")));
        //모든 헤더 다 뚫어주는 것
        config.setAllowedHeaders(List.of("*"));
        //노출된 헤더 다 허용
        config.setExposedHeaders(List.of("*"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        //모든 매핑 주소에 대해서 위의 설정을 먹이겠다.
        source.registerCorsConfiguration("/**", config);
        return source;
    }
    @Bean
    protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        //csrf를 비활성화
        http.csrf(AbstractHttpConfigurer::disable)
                .formLogin(AbstractHttpConfigurer::disable)
                .httpBasic(AbstractHttpConfigurer::disable)
                //시큐리티한테 cors 설정을 보내는 것
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSource()))
                //csrf를 비활성화 했기 때문에 STATELESS로 해줌
                .sessionManagement((sessionManagement) ->
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                //인가해주는 것
                //요청에 대해서 인가를 해준다.
                .authorizeHttpRequests((authorizeRequests) ->
                        authorizeRequests
                                .requestMatchers("/v3/**", "/swagger-ui/**").permitAll()
                                //인가에 대한 옵션 (권한에 대한 옵션)
                                //
                                .requestMatchers(HttpMethod.GET,"/exception/**").permitAll()
                                //로그인 모두가 가능하게 열어두는 것
                                .requestMatchers("v1/member/login/**").permitAll()
                                .requestMatchers("v1/member/join/**").permitAll()
                                //admin의 공간 옵션
                                .requestMatchers("v1/auth-test/test-admin").hasAnyRole("ADMIN")
                                //그외의 공간
                                .anyRequest().hasRole("ADMIN")
                );
        // 잘못된 접근이 경우 핸들러한테 에러를 넘긴다.
        http.exceptionHandling(handler -> handler.accessDeniedHandler(accessDenied));
        http.exceptionHandling(handler -> handler.authenticationEntryPoint(entryPoint));
        //수행하기 전에 프로바이더한테 유저네임
        //필터는 단계별 조금씩 걸러내주는 내용이다. .class는 필터에 대한 자리라고 생각하면 된다.
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);

        return http.build();

    }



}
