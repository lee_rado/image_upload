package com.episode6.myworkerapi.configure;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {
    //CustomAuthenticationEntryPoint는 인증이 필요한 리소스에 대한 접근이 시도되었을 때 발생하는 예외를 처리

    // 로그인을 했으나 그 권한을 가지지 않은 사람을 거부하는 것
    // 권한에 대한 체크
    // 강의 수강한 학생이 선생님 자리에 앉는 것
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.sendRedirect("/exception//entry-point");

    }
}
