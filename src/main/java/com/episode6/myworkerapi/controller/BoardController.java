package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.board.BoardChangePostRequest;
import com.episode6.myworkerapi.model.board.BoardItem;
import com.episode6.myworkerapi.model.board.BoardRequest;
import com.episode6.myworkerapi.model.board.BoardResponse;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;
    private final MemberService memberService;
    private final DeleteService deleteService;

    @PostMapping("/join/member-id/{memberId}")
    @Operation(summary = "게시글 등록")
    public CommonResult setBoard(@RequestBody BoardRequest request, @PathVariable long memberId){

        Member member = memberService.getMemberData(memberId);

        boardService.setBoard(request, member);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "게시물 10개당 한 페이지 최신순")
    public ListResult<BoardItem> getBoardPage(@PathVariable int pageNum) {
        return ResponseService.getListResult(boardService.getBoardPage(pageNum),true);
    }

    @GetMapping("all/integration/{pageNum}")
    @Operation(summary = "통합 카테고리 게시물 페이징")
    public ListResult<BoardItem> getIntegrationPage(@PathVariable int pageNum) {
        return ResponseService.getListResult(boardService.getIntegrationPage(pageNum),true);
    }
    @GetMapping("all/boss/{pageNum}")
    @Operation(summary = "사장님 카테고리 게시물 페이징")
    public ListResult<BoardItem> getBossPage(@PathVariable int pageNum) {
        return ResponseService.getListResult(boardService.getBossPage(pageNum),true);
    }
    @GetMapping("all/job/{pageNum}")
    @Operation(summary = "알바생 카테고리 게시물 페이징")
    public ListResult<BoardItem> getJobPage(@PathVariable int pageNum) {
        return ResponseService.getListResult(boardService.getJobPage(pageNum),true);
    }


    @GetMapping("detail/board-id/{boardId}")
    @Operation(summary = "게시물 상세보기")
    public SingleResult<BoardResponse> getBoard(@PathVariable long boardId) {
        return ResponseService.getSingleResult(boardService.getBoard(boardId));
    }

    @PutMapping("put/board-id/{boardId}")
    @Operation(summary = "게시물 수정")
    public CommonResult putBoard(@PathVariable long boardId, @RequestBody BoardChangePostRequest request) {
        boardService.putBoard(boardId, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("del/board-id/{boardId}")
    @Operation(summary = "게시물 삭제")
    public CommonResult delBoard(@PathVariable long boardId) {
        deleteService.delBoard(boardId);

        return ResponseService.getSuccessResult();
    }
}
