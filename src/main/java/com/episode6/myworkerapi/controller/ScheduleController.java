package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.schedule.ScheduleEndWorkRequest;
import com.episode6.myworkerapi.model.schedule.ScheduleItem;
import com.episode6.myworkerapi.model.schedule.ScheduleRequest;
import com.episode6.myworkerapi.service.BusinessMemberService;
import com.episode6.myworkerapi.service.BusinessService;
import com.episode6.myworkerapi.service.ResponseService;
import com.episode6.myworkerapi.service.ScheduleService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/schedule")
public class ScheduleController {
    private final BusinessService businessService;
    private final BusinessMemberService businessMemberService;
    private final ScheduleService scheduleService;

    @PostMapping("/join/business-member-id/{businessMemberId}")
    @Operation(summary = "출근 등록")
    public CommonResult setSchedule(@PathVariable long businessMemberId, @RequestBody ScheduleRequest request) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        scheduleService.setSchedule(businessMember, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "출근 리스트 보기 관리자용")
    public ListResult<ScheduleItem> getSchedules(@PathVariable int pageNum) {
        return ResponseService.getListResult(scheduleService.getSchedules(pageNum), true);
    }

    @GetMapping("/all/business-id/{businessId}/{pageNum}")
    @Operation(summary = "사업장 알바생들 출퇴근 보기")
    public ListResult<ScheduleItem> getScheduleBusiness(@PathVariable long businessId, @PathVariable int pageNum) {
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(scheduleService.getScheduleBusiness(business, pageNum), true);
    }

    @GetMapping("/all/business-member-id/{businessMemberId}/{pageNum}")
    @Operation(summary = "알바생 출퇴근 리스트 보기")
    public ListResult<ScheduleItem> getScheduleBusinessMember(@PathVariable long businessMemberId, @PathVariable int pageNum) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        return ResponseService.getListResult(scheduleService.getScheduleBusinessMember(businessMember, pageNum), true);
    }

    @PutMapping("/end-work/business-member-id/{businessMemberId}")
    @Operation(summary = "퇴근 하기")
    public CommonResult putEndWork(@PathVariable long businessMemberId, @RequestBody ScheduleEndWorkRequest request) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        scheduleService.putEndWork(businessMember, request);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/start-recess/business-member-id/{businessMemberId}")
    @Operation(summary = "휴식 시작")
    public CommonResult putRecessStart(@PathVariable long businessMemberId) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        scheduleService.putRecessStart(businessMember);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{scheduleId}")
    @Operation(summary = "출퇴근 삭제")
    public CommonResult delSchedule(@PathVariable long scheduleId) {
        scheduleService.delSchedule(scheduleId);
        return ResponseService.getSuccessResult();
    }
}
