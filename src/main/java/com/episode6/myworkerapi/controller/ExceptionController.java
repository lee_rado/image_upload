package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    //예외 처리를 위한 컨트롤러를 작성
    @GetMapping("/access-denied")
    ///access-denied 엔드포인트는 권한이 없는 사용자가 보호된 리소스에 액세스할 때 발생하는 예외를 처리
    public CommonResult accessDeniedException() {
        throw new CMemberPasswordException();
    }

    @GetMapping("/entry-point")
    ///entry-point 엔드포인트는 인증된 사용자가 요청한 리소스에 대한 권한이 없을 때 발생하는 예외를 처리
    public CommonResult entryPointException() {
        throw new CMemberPasswordException();
    }
}
