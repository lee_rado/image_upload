package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.login.LoginRequest;
import com.episode6.myworkerapi.model.login.LoginResponse;
import com.episode6.myworkerapi.service.LoginService;
import com.episode6.myworkerapi.service.ResponseService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/login")
@RequiredArgsConstructor
public class LoginController {
    private final LoginService loginService;

    //관리자용 웹
    @PostMapping("/web/admin")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_MANAGEMENT, loginRequest, "WEB"));
    }

    //사업자용 웹
    @PostMapping("/web/boss")
    public SingleResult<LoginResponse> doLoginBossWeb(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_BOSS, loginRequest, "WEB"));
    }

    //사업자용 앱
    @PostMapping("/app/boss")
    public SingleResult<LoginResponse> doLoginBossApp(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_BOSS, loginRequest,"APP"));
    }

    //알바생용 앱
    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_GENERAL, loginRequest,"APP"));
    }
}
