package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.productrecord.ProductRecordItem;
import com.episode6.myworkerapi.model.productrecord.ProductRecordRequest;
import com.episode6.myworkerapi.service.BusinessMemberService;
import com.episode6.myworkerapi.service.ProductRecordService;
import com.episode6.myworkerapi.service.ProductService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product-record")
public class ProductRecordController {
    private final ProductService productService;
    private final BusinessMemberService businessMemberService;
    private final ProductRecordService productRecordService;

    @PostMapping("/join/product-id/{productId}/business-member-id/{businessMemberId}")
    @Operation(summary = "재고 수정 등록")
    public CommonResult setProductRecord(@PathVariable long productId, @PathVariable long businessMemberId, @RequestBody ProductRecordRequest request) {
        Product product = productService.getProductData(productId);
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        productRecordService.setProductRecord(product, businessMember, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "재고 수정 리스트 관리자용?")
    public ListResult<ProductRecordItem> getProductRecords(@PathVariable int pageNum) {
        return ResponseService.getListResult(productRecordService.getProductRecords(pageNum), true);
    }
}
