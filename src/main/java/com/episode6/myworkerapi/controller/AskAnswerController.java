package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.MemberAsk;
import com.episode6.myworkerapi.model.askanswer.AskAnswerItem;
import com.episode6.myworkerapi.model.askanswer.AskAnswerRequest;
import com.episode6.myworkerapi.model.askanswer.AskAnswerResponse;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.AskAnswerService;
import com.episode6.myworkerapi.service.MemberAskService;
import com.episode6.myworkerapi.service.MemberService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/ask-answer")
public class AskAnswerController {
    private final MemberService memberService;
    private final MemberAskService memberAskService;
    private final AskAnswerService askAnswerService;

    @PostMapping("/join/member-id/{memberId}/member-ask-id/{memberAskId}")
    @Operation(summary = "문의 내용 답변 등록")
    public CommonResult setAskAnswer(@PathVariable long memberId, @PathVariable long memberAskId, @RequestBody AskAnswerRequest request) {
        Member member = memberService.getMemberData(memberId);
        MemberAsk memberAsk = memberAskService.getMemberAskData(memberAskId);
        askAnswerService.setAskAnswer(member, memberAsk, request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "문의 답변 리스트")
    public ListResult<AskAnswerItem> getAskAnswers(@PathVariable int pageNum) {
        return ResponseService.getListResult(askAnswerService.getAskAnswers(pageNum),true);
    }

    @GetMapping("/detail/ask-answer-id/{askAnswerId}")
    @Operation(summary = "문의 답변 상세보기")
    public SingleResult<AskAnswerResponse> getAskAnswer(@PathVariable long askAnswerId) {
        return ResponseService.getSingleResult(askAnswerService.getAskAnswer(askAnswerId));
    }

    @GetMapping("/detail/member-ask-id/{memberAskId}")
    @Operation(summary = "문의 스퀀스로 답변 상세보기")
    public SingleResult<AskAnswerResponse> getAskAnswerMemberAsk(@PathVariable long memberAskId) {
        return ResponseService.getSingleResult(askAnswerService.getAskAnswerMemberAsk(memberAskId));
    }
}
