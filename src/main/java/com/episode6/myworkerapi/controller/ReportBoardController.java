package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.report.ReportBoardItem;
import com.episode6.myworkerapi.model.report.ReportRequest;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/report-board")
public class ReportBoardController {
    private final ReportBoardService reportBoardService;
    private final MemberService memberService;
    private final BoardService boardService;


    @PostMapping("join/member-id/{memberId}/board-id/{boardId}/")
    @Operation(summary = "게시글 신고 중복신고 불가능")
    public CommonResult setReportBoard(@RequestBody ReportRequest request, @PathVariable long memberId, @PathVariable long boardId) throws Exception {

        Member member = memberService.getMemberData(memberId);
        Board board = boardService.getBoardData(boardId);
        reportBoardService.setReportBoard(member,board,request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "게시글 신고 최신순 페이징")
    public ListResult<ReportBoardItem> setReportBoardPage(@PathVariable int pageNum) {
        return ResponseService.getListResult(reportBoardService.setReportBoardPage(pageNum),true);
    }

    @DeleteMapping("del/board-id/{boardId}")
    @Operation(summary = "신고 게시물 삭제")
    public CommonResult delReportBoard(@PathVariable long boardId) {

        reportBoardService.delReportBoard(boardId);

        return ResponseService.getSuccessResult();
    }
}
