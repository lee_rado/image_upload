package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MemberState {
    NORMAL_MEMBER("정상 회원"),
    WAIT_MEMBER("가입대기중"),
    STOP_MEMBER("활동중지"),
    OUT_MEMBER("회원탈퇴");

    private final String name;

}
