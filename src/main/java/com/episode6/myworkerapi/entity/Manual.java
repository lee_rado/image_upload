package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.manual.ManualRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Manual {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "businessMemberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private BusinessMember businessMember;

    @Column(nullable = false)
    private LocalDate dateManual;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    private String manualImgUrl;

    private Manual(Builder builder) {
        this.businessMember = builder.businessMember;
        this.dateManual = builder.dateManual;
        this.title = builder.title;
        this.content = builder.content;
        this.manualImgUrl = builder.manualImgUrl;
    }


    public static class Builder implements CommonModelBuilder<Manual>{
        private final BusinessMember businessMember;
        private final LocalDate dateManual;
        private final String title;
        private final String content;
        private final String manualImgUrl;

        public Builder(ManualRequest manualRequest,BusinessMember businessMember) {
            this.businessMember = businessMember;
            this.dateManual = manualRequest.getDateManual();
            this.title = manualRequest.getTitle();
            this.content = manualRequest.getContent();
            this.manualImgUrl = manualRequest.getManualImgUrl();
        }
        @Override
        public Manual build() {
            return new Manual(this);
        }
    }
}
