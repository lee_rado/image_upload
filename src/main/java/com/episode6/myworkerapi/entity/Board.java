package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.enums.Category;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.board.BoardChangePostRequest;
import com.episode6.myworkerapi.model.board.BoardRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private Category category;

    @JoinColumn(name = "memberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Member member;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String content;

    private String boardImgUrl;

    @Column(nullable = false)
    private LocalDateTime dateBoard;

    /**
     * 게시글 수정
     */
    public void putBoard(BoardChangePostRequest request) {
            this.title = request.getTitle();
            this.content = request.getContent();
            this.boardImgUrl = request.getBoardImgUrl();
    }


    private Board(Builder builder) {
        this.member = builder.member;
        this.title = builder.title;
        this.content = builder.content;
        this.boardImgUrl = builder.boardImgUrl;
        this.category = builder.category;
        this.dateBoard = builder.dateBoard;
    }

    public static class Builder implements CommonModelBuilder<Board> {
        private final Member member;
        private final String title;
        private final String content;
        private final String boardImgUrl;
        private final Category category;
        private final LocalDateTime dateBoard;

        public Builder(BoardRequest boardRequest, Member member) {
            this.member = member;
            this.title = boardRequest.getTitle();
            this.content = boardRequest.getContent();
            this.boardImgUrl = boardRequest.getBoardImgUrl();
            this.category = boardRequest.getCategory();
            this.dateBoard = LocalDateTime.now();
        }

        @Override
        public Board build() {
            return new Board(this);
        }
    }

}
