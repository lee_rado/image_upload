package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.product.ProductChangeRequest;
import com.episode6.myworkerapi.model.product.ProductRequest;
import com.episode6.myworkerapi.model.productrecord.ProductRecordRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "businessMemberId",nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private BusinessMember businessMember;

    @Column(nullable = false)
    private LocalDateTime dateProduct;

    @Column(nullable = false, length = 25)
    private String productName;

    @Column(nullable = false)
    private Short minQuantity;

    @Column(nullable = false)
    private Short nowQuantity;

    public void putProduct(ProductChangeRequest request) {
        this.productName = request.getProductName();
        this.minQuantity = request.getMinQuantity();
    }
    public void putProduct(BusinessMember businessMember, ProductRecordRequest request) {
        this.businessMember = businessMember;
        this.nowQuantity = request.getNowQuantity();
        this.dateProduct = request.getDateProductRecord();
    }

    private Product(Builder builder) {
        this.businessMember = builder.businessMember;
        this.dateProduct = builder.dateProduct;
        this.productName = builder.productName;
        this.minQuantity = builder.minQuantity;
        this.nowQuantity = builder.nowQuantity;
    }

    public static class Builder implements CommonModelBuilder<Product> {
        private final BusinessMember businessMember;
        private final LocalDateTime dateProduct;
        private final String productName;
        private final Short minQuantity;
        private final Short nowQuantity;

        public Builder(BusinessMember businessMember, ProductRequest request) {
            this.businessMember = businessMember;
            this.dateProduct = LocalDateTime.now();
            this.productName = request.getProductName();
            this.minQuantity = request.getMinQuantity();
            this.nowQuantity = request.getNowQuantity();
        }

        @Override
        public Product build() {
            return new Product(this);
        }
    }
}
