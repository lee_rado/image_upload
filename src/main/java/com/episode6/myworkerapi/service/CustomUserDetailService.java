package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor

//Spring Security에서 제공하는 UserDetailsService 인터페이스를 구현하여 사용자 정보를 가져오는 서비스를 작성
public class CustomUserDetailService implements UserDetailsService {
    private final MemberRepository memberRepository;
    @Override
    //loadUserByUsername() 메서드를 구현하여 사용자 이름을 기반으로 Member 엔티티를 조회하고, 해당 회원의 정보를 UserDetails로 반환
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member member = memberRepository.findByUsername(username).orElseThrow(CMemberPasswordException::new);
        //만약 사용자를 찾지 못하면 UsernameNotFoundException을 발생
        return member;
    }
}
