package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.lib.CommonFile;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class FilesService {
    public void imageResize(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.mltipartToFile(multipartFile);
        BufferedImage bufferedImage = ImageIO.read(file);


        int oldImageWidth = bufferedImage.getWidth();
        int oldImageHeight = bufferedImage.getHeight();
        int newImageWidth;
        int newImageHeight;

        if (oldImageWidth > oldImageHeight) {
            newImageWidth = 1000;
            newImageHeight = 1000 * oldImageHeight / oldImageWidth;
        } else {
            newImageWidth = 1000 * oldImageWidth / oldImageHeight;
            newImageHeight = 1000;
        }
        Image resizedImage = bufferedImage.getScaledInstance(newImageWidth, newImageHeight, Image.SCALE_SMOOTH);
        BufferedImage newImage = new BufferedImage(newImageWidth, newImageHeight, bufferedImage.TYPE_INT_RGB);
        Graphics graphics = newImage.getGraphics();
        graphics.drawImage(resizedImage,0,0,null);
        graphics.dispose();

        File newFile = new File("");
        ImageIO.write(newImage, multipartFile.getOriginalFilename(), newFile);

    }

}
