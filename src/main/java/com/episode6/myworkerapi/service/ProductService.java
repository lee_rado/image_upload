package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.product.ProductChangeRequest;
import com.episode6.myworkerapi.model.product.ProductItem;
import com.episode6.myworkerapi.model.product.ProductRequest;
import com.episode6.myworkerapi.model.product.ProductResponse;
import com.episode6.myworkerapi.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public Product getProductData(long id) {
        return productRepository.findById(id).orElseThrow();
    }
    /**
     * 상품 재고 최소 기준 등록
     */
    public void setProduct(BusinessMember businessMember, ProductRequest request) {
        productRepository.save(new Product.Builder(businessMember, request).build());
    }

    /**
     * 상품 재고 리스트 관리자 전체다 보기
     */
    public ListResult<ProductItem> getProductsAdmin(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Product> products = productRepository.findAllByOrderByIdDesc(pageRequest);

        List<ProductItem> result = new LinkedList<>();
        for (Product product : products) result.add(new ProductItem.Builder(product).build());
        return ListConvertService.settingResult(result);
    }

    /**
     * 사업장 상품 재고 리스트
     */
    public ListResult<ProductItem> getProducts(Business business, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Product> products = productRepository.findAllByBusinessMember_BusinessOrderByIdDesc(business ,pageRequest);

        List<ProductItem> result = new LinkedList<>();
        for (Product product : products) result.add(new ProductItem.Builder(product).build());
        return ListConvertService.settingResult(result, products.getTotalElements(), products.getTotalPages(), products.getPageable().getPageNumber());
    }

    /**
     * 상품 재고 상세보기
     */
    public ProductResponse getProduct(long id) {
        Product product = productRepository.findById(id).orElseThrow();
        return new ProductResponse.Builder(product).build();
    }

    /**
     * 상품 재고 이름 최소수량 수정
     */
    public void putProduct(long id, ProductChangeRequest request) {
        Product originData = productRepository.findById(id).orElseThrow();
        originData.putProduct(request);
        productRepository.save(originData);
    }
}
