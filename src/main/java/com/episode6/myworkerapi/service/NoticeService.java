package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.Notice;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.notice.NoticeChangeRequest;
import com.episode6.myworkerapi.model.notice.NoticeItem;
import com.episode6.myworkerapi.model.notice.NoticeRequest;
import com.episode6.myworkerapi.model.notice.NoticeResponse;
import com.episode6.myworkerapi.repository.NoticeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NoticeService {
    private final NoticeRepository noticeRepository;

    /**
     *
     * @param request 공지사항 등록
     */
    public void setNotice(Member member, NoticeRequest request) {
        noticeRepository.save(new Notice.Builder(member, request).build());
    }

    /**
     *
     * @param pageNum 공지사항 리스트 보기
     * @return 페이징
     */
    public ListResult<NoticeItem> getNotices(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Notice> notices = noticeRepository.findAllByOrderByIdDesc(pageRequest);

        List<NoticeItem> result = new LinkedList<>();
        for (Notice notice : notices) result.add(new NoticeItem.Builder(notice).build());
        return ListConvertService.settingResult(result, notices.getTotalElements(), notices.getTotalPages(), notices.getPageable().getPageNumber());
    }

    /**
     *
     * @param id 공지사항 상세보기
     * @return 단수
     */
    public NoticeResponse getNotice(long id) {
        Notice originData = noticeRepository.findById(id).orElseThrow();
        return new NoticeResponse.Builder(originData).build();
    }

    /**
     *
     * @param id 공지사항 수정
     * @param request 수정내역
     */
    public void putNotice(long id, NoticeChangeRequest request) {
        Notice originData = noticeRepository.findById(id).orElseThrow();
        originData.putNotice(request);
        noticeRepository.save(originData);
    }

    /**
     *
     * @param id 공지사항 삭제
     */
    public void delNotice(long id) {
        noticeRepository.deleteById(id);
    }
}
