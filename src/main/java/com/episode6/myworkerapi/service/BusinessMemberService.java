package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.businessmember.*;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.BusinessMemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BusinessMemberService {
    private final BusinessMemberRepository businessMemberRepository;

    /**
     *
     * @param id 아이디값 가져오기
     */
    public BusinessMember getBusinessMemberData(long id) {
        return businessMemberRepository.findById(id).orElseThrow();
    }

    /**
     *
     * @param business 사업장
     * @param member 회원
     * @param request 이미 일하는 상태로 등록되어 있으면 다시 등록불가
     */
    public void setBusinessMember(Business business, Member member, BusinessMemberRequest request) {
        Optional<BusinessMember> businessMember = businessMemberRepository.findByMemberAndBusinessAndIsWork(member, business, true);

        if (businessMember.isEmpty()) businessMemberRepository.save(new BusinessMember.Builder(member, business, request).build());
    }

    /**
     *
     * @param pageNum 사업장 직원 리스트 최신순 관리자용
     * @return 최근에 가입한 알바부터 위에서 나온다.
     */
    public ListResult<BusinessMemberItem> getBusinessMemberAll(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<BusinessMember> businessMembers = businessMemberRepository.findAllByOrderByIdDesc(pageRequest);

        List<BusinessMemberItem> result = new LinkedList<>();
        for (BusinessMember businessMember : businessMembers) result.add(new BusinessMemberItem.Builder(businessMember).build());
        return ListConvertService.settingResult(result, businessMembers.getTotalElements(), businessMembers.getTotalPages(), businessMembers.getPageable().getPageNumber());
    }

    /**
     *
     * @param business 사장님 사업장 알바생만 보기
     * @param pageNum 페이징 재직중인사람부터 퇴직자까지 보여줌
     */
    public ListResult<BusinessMemberItem> getBusinessOnlyMember(Business business,int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<BusinessMember> businessMembers = businessMemberRepository.findAllByBusinessOrderByIsWorkDesc(business, pageRequest);

        List<BusinessMemberItem> result = new LinkedList<>();
        for (BusinessMember businessMember : businessMembers) result.add(new BusinessMemberItem.Builder(businessMember).build());
        return ListConvertService.settingResult(result, businessMembers.getTotalElements(), businessMembers.getTotalPages(), businessMembers.getPageable().getPageNumber());
    }

    /**
     *
     * @param business 사장님 사업장 알바생만 보기
     * @param pageNum 페이징 재직중인사람만 보여줌
     * @return
     */
    public ListResult<BusinessMemberItem> getBusinessOnlyMemberIsWork(Business business,int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<BusinessMember> businessMembers = businessMemberRepository.findAllByBusinessAndIsWorkOrderByIdDesc(business, true, pageRequest);

        List<BusinessMemberItem> result = new LinkedList<>();
        for (BusinessMember businessMember : businessMembers) result.add(new BusinessMemberItem.Builder(businessMember).build());
        return ListConvertService.settingResult(result, businessMembers.getTotalElements(), businessMembers.getTotalPages(), businessMembers.getPageable().getPageNumber());
    }

    /**
     *
     * @param id 사업장 알바생 상세보기
     * @return 단수
     */
    public BusinessMemberResponse getBusinessMember(long id) {
        BusinessMember originData = businessMemberRepository.findById(id).orElseThrow();
        return new BusinessMemberResponse.Builder(originData).build();
    }

    /**
     *
     * @param id 알바생 재직 퇴직 수정
     */
    public void putMemberChangeWork(long id, BusinessMemberChangeWorkRequest request) {
        BusinessMember originData = businessMemberRepository.findById(id).orElseThrow();

        originData.putChangeIsWork(request);
        businessMemberRepository.save(originData);
    }

    /**
     *
     * @param id 알바생 근무일정 등록 및 수정
     */
    public void putMemberSchedule(long id, BusinessMemberScheduleRequest request) {
        BusinessMember originData = businessMemberRepository.findById(id).orElseThrow();

        originData.putMemberSchedule(request);
        businessMemberRepository.save(originData);
    }
}
