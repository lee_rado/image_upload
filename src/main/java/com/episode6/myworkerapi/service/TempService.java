package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.lib.CommonFile;
import com.episode6.myworkerapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
@Service
@RequiredArgsConstructor
public class TempService {
    private final MemberRepository memberRepository;

    /**
     *
     * @param multipartFile 데이터 베이스에 회원 정보
     * @throws IOException 한번에 업로드
     */
    public void setMemberByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.mltipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line = "";

        int index = 0;

        while ((line = bufferedReader.readLine()) != null) {
            if (index > 0) {
                String[] cols = line.split(",");
                if (cols.length == 9) {
                    memberRepository.save(new Member.BuilderCsv(cols).build());
                }
            }
            index++;
        }
        bufferedReader.close();
    }
}
