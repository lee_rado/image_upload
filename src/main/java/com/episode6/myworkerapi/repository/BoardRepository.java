package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.enums.Category;
import com.episode6.myworkerapi.model.board.BoardItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board, Long> {

    /**
     * (페이징) 게시물 최신순 정렬
     */
    Page<Board> findAllByOrderByIdDesc(Pageable pageable);

    /**
     * 카테고리별 페이징
     */
    Page<Board> findAllByCategoryOrderByIdDesc(Pageable pageable, Category category);
}
