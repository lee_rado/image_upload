package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.MemberType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByUsername(String username);

    Page<Member> findAllByOrderByIdDesc(Pageable pageable);


    Page<Member> findAllByMemberTypeOrderByIdDesc(Pageable pageable, MemberType memberType);

    //findByUsername() 메서드를 통해 사용자의 이름(username)으로 회원 정보를 조회할 수 있도록 한다.
    Optional<Member> findByUsername(String username);
}
