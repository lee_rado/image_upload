package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    Optional<Schedule> findByBusinessMemberAndDateWork(BusinessMember businessMember, LocalDate localDate);

    Schedule findByDateWorkAndBusinessMember(LocalDate localDate, BusinessMember businessMember);

    Page<Schedule> findAllByOrderByIdDesc(Pageable pageable);
    Page<Schedule> findAllByBusinessMember_BusinessOrderByIdDesc(Business business, Pageable pageable);
    Page<Schedule> findAllByBusinessMemberOrderByIdDesc(BusinessMember businessMember, Pageable pageable);
}
