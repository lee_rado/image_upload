package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Request;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RequestRepository extends JpaRepository<Request, Long> {


    /**
     * 중복불가를 위해 비즈니스 아이디 찾기
     */
    Optional<Request> findByBusiness(Business business);

    /**
     * 등록 요청 최신순 보기
     */
    Page<Request> findAllByOrderByIdDesc(Pageable pageable);
}
