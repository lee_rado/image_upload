package com.episode6.myworkerapi.model.schedule;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ScheduleRequest {
    private Double startLatitude;
    private Double startLongitude;
    private String etc;
    private Boolean isLateness;
}
