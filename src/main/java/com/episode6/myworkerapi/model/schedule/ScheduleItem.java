package com.episode6.myworkerapi.model.schedule;

import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ScheduleItem {
    private Long id;
    private Long businessMemberId;
    private String businessMemberName;
    private LocalDate dateWork;
    private LocalTime startWork;
    private LocalTime endWork;
    private Double restTime;
    private Double plusWorkTime;
    private Double outWorkTime;
    private Double totalWorkTime;
    private String isLateness;
    private Boolean isOverTime;
    private String etc;

    private ScheduleItem(Builder builder) {
        this.id = builder.id;
        this.businessMemberId = builder.businessMemberId;
        this.businessMemberName = builder.businessMemberName;
        this.dateWork = builder.dateWork;
        this.startWork = builder.startWork;
        this.endWork = builder.endWork;
        this.restTime = builder.restTime;
        this.plusWorkTime = builder.plusWorkTime;
        this.outWorkTime = builder.outWorkTime;
        this.totalWorkTime = builder.totalWorkTime;
        this.isLateness = builder.isLateness;
        this.isOverTime = builder.isOverTime;
        this.etc = builder.etc;
    }

    public static class Builder implements CommonModelBuilder<ScheduleItem> {
        private final Long id;
        private final Long businessMemberId;
        private final String businessMemberName;
        private final LocalDate dateWork;
        private final LocalTime startWork;
        private final LocalTime endWork;
        private final Double restTime;
        private final Double plusWorkTime;
        private final Double outWorkTime;
        private final Double totalWorkTime;
        private final String isLateness;
        private final Boolean isOverTime;
        private final String etc;

        public Builder(Schedule schedule) {
            this.id = schedule.getId();
            this.businessMemberId = schedule.getBusinessMember().getId();
            this.businessMemberName = schedule.getBusinessMember().getMember().getName();
            this.dateWork = schedule.getDateWork();
            this.startWork = schedule.getStartWork();
            this.endWork = schedule.getEndWork();
            this.restTime = schedule.getRestTime();
            this.plusWorkTime = schedule.getPlusWorkTime();
            this.outWorkTime = schedule.getOutWorkTime();
            this.totalWorkTime = schedule.getTotalWorkTime();
            this.isLateness = schedule.getIsLateness() ? "지각" : "정상출근";
            this.isOverTime = schedule.getIsOverTime();
            this.etc = schedule.getEtc();
        }
        @Override
        public ScheduleItem build() {
            return new ScheduleItem(this);
        }
    }
}
