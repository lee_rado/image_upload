package com.episode6.myworkerapi.model.board;


import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardChangePostRequest {
    private String title;
    private String content;
    private String boardImgUrl;
}
