package com.episode6.myworkerapi.model.manual;


import com.episode6.myworkerapi.entity.BusinessMember;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ManualRequest {
    private LocalDate dateManual;
    private String title;
    private String content;
    private String manualImgUrl;
}
