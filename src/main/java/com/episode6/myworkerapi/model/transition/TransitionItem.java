package com.episode6.myworkerapi.model.transition;

import com.episode6.myworkerapi.entity.Transition;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TransitionItem {
    private Long id;
    private Long businessMemberId;
    private String businessMemberName;
    private String content;
    private LocalDateTime dateTransition;
    private String isFinish;
    private LocalDateTime dateFinish;

    private TransitionItem(Builder builder) {
        this.id = builder.id;
        this.businessMemberId = builder.businessMemberId;
        this.businessMemberName = builder.businessMemberName;
        this.content = builder.content;
        this.dateTransition = builder.dateTransition;
        this.isFinish = builder.isFinish;
        this.dateFinish = builder.dateFinish;
    }

    public static class Builder implements CommonModelBuilder<TransitionItem> {
        private final Long id;
        private final Long businessMemberId;
        private final String businessMemberName;
        private final String content;
        private final LocalDateTime dateTransition;
        private final String isFinish;
        private final LocalDateTime dateFinish;

        public Builder(Transition transition) {
            this.id = transition.getId();
            this.businessMemberId = transition.getBusinessMember().getId();
            this.businessMemberName = transition.getBusinessMember().getMember().getName();
            this.content = transition.getContent();
            this.dateTransition = transition.getDateTransition();
            this.isFinish = transition.getIsFinish() ? "처리 완료" : "미처리";
            this.dateFinish = transition.getDateFinish();
        }
        @Override
        public TransitionItem build() {
            return new TransitionItem(this);
        }
    }
}
