package com.episode6.myworkerapi.model.notice;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeRequest {
    private String title;
    private String content;
    private String noticeImgUrl;
}
