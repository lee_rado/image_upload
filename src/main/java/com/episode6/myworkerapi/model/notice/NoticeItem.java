package com.episode6.myworkerapi.model.notice;

import com.episode6.myworkerapi.entity.Notice;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NoticeItem {
    private Long id;
    private Long memberId;
    private String memberType;
    private String title;
    private LocalDateTime dateNotice;

    private NoticeItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberType = builder.memberType;
        this.title = builder.title;
        this.dateNotice = builder.dateNotice;
    }

    public static class Builder implements CommonModelBuilder<NoticeItem> {
        private final Long id;
        private final Long memberId;
        private final String memberType;
        private final String title;
        private final LocalDateTime dateNotice;

        public Builder(Notice notice) {
            this.id = notice.getId();
            this.memberId = notice.getMember().getId();
            this.memberType = notice.getMember().getMemberType().getName();
            this.title = notice.getTitle();
            this.dateNotice = notice.getDateNotice();
        }
        @Override
        public NoticeItem build() {
            return new NoticeItem(this);
        }
    }
}
