package com.episode6.myworkerapi.model.memberask;

import com.episode6.myworkerapi.entity.MemberAsk;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberAskItem {
    private Long id;
    private Long memberId;
    private String memberName;
    private String isAnswer;
    private String title;
    private LocalDateTime dateMemberAsk;

    private MemberAskItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.isAnswer = builder.isAnswer;
        this.title = builder.title;
        this.dateMemberAsk = builder.dateMemberAsk;
    }

    public static class Builder implements CommonModelBuilder<MemberAskItem> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final String isAnswer;
        private final String title;
        private final LocalDateTime dateMemberAsk;

        public Builder(MemberAsk memberAsk) {
            this.id = memberAsk.getId();
            this.memberId = memberAsk.getMember().getId();
            this.memberName = memberAsk.getMember().getName();
            this.isAnswer = memberAsk.getIsAnswer() ? "답변 완료" : "미답변";
            this.title = memberAsk.getTitle();
            this.dateMemberAsk = memberAsk.getDateMemberAsk();
        }
        @Override
        public MemberAskItem build() {
            return new MemberAskItem(this);
        }
    }
}
