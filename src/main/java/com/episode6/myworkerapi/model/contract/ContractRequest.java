package com.episode6.myworkerapi.model.contract;


import com.episode6.myworkerapi.enums.Insurance;
import com.episode6.myworkerapi.enums.PayType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ContractRequest {

    private LocalDate dateContract;
    private PayType payType;
    private Double payPrice;
    private LocalDate dateWorkStart;
    private LocalDate dateWorkEnd;
    private Short restTime;
    private Insurance insurance;
    private String workDay;
    private Short workTime;
    private Short weekWorkTime;
    private Boolean isWeekPay;
    private Double mealPay;
    private String wagePayment;
    private String wageAccountNumber;
    private String contractCopyImgUrl;

}
