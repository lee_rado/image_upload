package com.episode6.myworkerapi.model.member;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberResponse {
    private Long id;
    private String memberState;
    private String memberType;
    private String name;
    private String username;
    private String isMan;
    private LocalDate dateBirth;
    private String phoneNumber;
    private String address;
    private LocalDateTime dateMember;

    private MemberResponse(Builder builder) {
        this.id = builder.id;
        this.memberState = builder.memberState;
        this.memberType = builder.memberType;
        this.name = builder.name;
        this.username = builder.username;
        this.isMan = builder.isMan;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.dateMember = builder.dateMember;
    }

    public static class Builder implements CommonModelBuilder<MemberResponse> {
        private final Long id;
        private final String memberState;
        private final String memberType;
        private final String name;
        private final String username;
        private final String isMan;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String address;
        private final LocalDateTime dateMember;

        public Builder(Member member) {
            this.id = member.getId();
            this.memberState = member.getMemberState().getName();
            this.memberType = member.getMemberType().getName();
            this.name = member.getName();
            this.username = member.getUsername();
            this.isMan = member.getIsMan() ? "남자" : "여자";
            this.dateBirth = member.getDateBirth();
            this.phoneNumber = member.getPhoneNumber();
            this.address = member.getAddress();
            this.dateMember = member.getDateMember();
        }
        @Override
        public MemberResponse build() {
            return new MemberResponse(this);
        }
    }
}
