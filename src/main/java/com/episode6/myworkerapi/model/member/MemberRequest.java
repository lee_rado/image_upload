package com.episode6.myworkerapi.model.member;

import com.episode6.myworkerapi.enums.MemberState;
import com.episode6.myworkerapi.enums.MemberType;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Getter
@Setter
public class MemberRequest {
    private MemberState memberState;
    private String name;
    @NotNull
    @Length(min = 5, max = 20)
    private String username;
    private String password;
    private String passwordRe;
    private Boolean isMan;
    private LocalDate dateBirth;
    private String phoneNumber;
    private String address;
}
