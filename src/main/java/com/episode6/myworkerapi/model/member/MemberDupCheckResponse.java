package com.episode6.myworkerapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberDupCheckResponse {
    private String isNew;
}
