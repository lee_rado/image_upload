package com.episode6.myworkerapi.model.business;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class BusinessChangeRequest {
    private String businessName;
    private String businessNumber;
    private String ownerName;
    private String businessImgUrl;
    private String businessType;
    private String businessLocation;
    private String businessEmail;
    private String businessPhoneNumber;
    private String reallyLocation;
    private Double latitudeBusiness;
    private Double longitudeBusiness;
    private Boolean isActivity;
    private LocalDate dateJoinBusiness;
    private Boolean isApprovalBusiness;
    private LocalDateTime dateApprovalBusiness;
    private String refuseReason;

}
