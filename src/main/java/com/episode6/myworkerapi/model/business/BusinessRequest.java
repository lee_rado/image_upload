package com.episode6.myworkerapi.model.business;


import com.episode6.myworkerapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class BusinessRequest {
    private String businessName;
    private String businessNumber;
    private String ownerName;
    private String businessImgUrl;
    private String businessType;
    private String businessLocation;
    private String businessEmail;
    private String businessPhoneNumber;
    private String ReallyLocation;
    private LocalDate dateJoinBusiness;

    private LocalDateTime dateApprovalBusiness;

}
